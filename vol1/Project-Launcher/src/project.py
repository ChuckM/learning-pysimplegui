"""Project dataclass
"""
from dataclasses import dataclass
import json as JSON


@dataclass
class Project():
    """
        The project object is a dataclass that contains the data
        associated with a single project.

        Attributes
        ----------
            name : str
                name of the project
            description : str
                brief description of the project
            location : str
                location (directory path) of the project
            commands : str
                command(s) to launch the appropriate tool(s)
    """

    name: str = ""
    description: str = ""
    location: str = ""
    commands: str = ""

    def to_dict(self):
        """returns attributes/values as a dict
        """
        return self.__dict__

    def to_json(self):
        """returns attributes/values as JSON-formatted dict
        """
        json_str = JSON.dumps(self.__dict__, indent=2)
        return json_str


if __name__ == "__main__":
    # prj = Project(name="Fred", description="Big Daddy",
    #               location="Bedrock", commands="ls -s")
    # print(f'{prj.to_json}')
    pass
