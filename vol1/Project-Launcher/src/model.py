"""Project Launcher data model
"""

import json as JSON
from project import Project


class Model:
    """Data model class definition
    """

    def __init__(self):
        """class initializer
        """
        self.projects = []
        self.load()

    def load(self):
        """load data
        """
        with open("src/data/projects.json", encoding='utf-8') as file:
            prjs = JSON.load(file)
            for obj in prjs:
                # print(f'{obj}')
                # name = obj["name"]
                # description = obj["description"]
                # location = obj["location"]
                # commands = obj["commands"]
                prj = Project()
                prj.__dict__ = obj
                self.projects.append(prj)

        # print(f'{self.projects}')

    def save(self):
        """save the project array to disk
        """
        # build the JSON string
        objstr = '['
        for obj in self.projects:
            # s_json = obj.to_json()
            objstr += obj.to_json() + ','
        objstr = objstr[:-1] + ']'
        with open("src/data/projects.json", 'w', encoding='utf-8') as file:
            file.write(objstr)

    def dump(self):
        """List the contents of the project dictionary
        """

        for obj in self.projects:
            print(f'name:\t{obj.name}')


if __name__ == '__main__':
    model = Model()
    model.save()
    model.dump()
