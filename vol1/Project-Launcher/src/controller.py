"""Project Launcher controller
"""


class Controller():
    """controller class for  Project Launcher application
    """
    models = {}

    def __init__(self, models: dict, views: dict):
        """class initializer

        Args:
            models (dict): _description_
            views (dict): _description_
        """
        self.models = models
        self.views = views
