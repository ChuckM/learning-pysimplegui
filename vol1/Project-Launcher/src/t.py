"""Project dataclass
"""
import json as JSON
from dataclasses import dataclass


@dataclass
class Project():
    """
        The project object is a dataclass that contains the data
        associated with a single project.

        Attributes
        ----------
            name : str
                name of the project
            description : str
                brief description of the project
            location : str
                location (directory path) of the project
            command : str
                command(s) to launch the appropriate tool(s)
    """

    name: str = ""
    description: str = ""
    location: str = ""
    command: str = ""

    def to_dict(self):
        """return attributes/values as a dict

        Returns:
            dict: dictionary of attributes and values
        """
        return self.__dict__

    def to_json(self):
        """return attributes/values as a JSON string

        Returns:
            dict: dict with the JSON representation of the
            attributes and values of the object.
        """
        return JSON.dumps(self.__dict__)


if __name__ == "__main__":
    prj = Project(name="First", description="desc",
                  location="~/", command="ls -l")
    print(f'{prj.to_dict()}')
    print(f'{prj.to_json()}')
