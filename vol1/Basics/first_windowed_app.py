"""
    Create a simple window
"""
import PySimpleGUI as sg

# define the layout for the main window
layout = [
    [sg.Text('Enter your name:'),
     sg.Input(key="-IN-")],
    [sg.Push(), sg.Text('', key='-OUTPUT-'), sg.Push()],
    [sg.Push(), sg.Button('Submit', key="+SUBMIT+"), sg.Exit()]
]

# open the window
window = sg.Window('First Windowed App', layout)

# start the event loop
while True:
    # read the event and values associated with the event
    event, values = window.read()

    # check to see if the user wants to close the window
    if event == sg.WIN_CLOSED or event == 'Exit':
        break

    # Handle the click event of the Submit button
    if event == "+SUBMIT+":
        # set the value of the output string
        str_val = f'Hello, {values["-IN-"]}' if len(values['-IN-']) > 0 else ''

        # refresh the window
        window['-OUTPUT-'].update(str_val)

# close the window
window.close()
